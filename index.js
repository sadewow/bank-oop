class Person {
    constructor(name, address) {
        this.name = name,
            this.address = address;
    }
}

class Security extends Person {
    constructor(name, address) {
        super(name, address);
    }

    register() {
        Customer.addQueueList(1)
    }
}

class Customer extends Person {
    constructor(name, address) {
        super(name, address),
            this.member = false;
        this.intention = "";
        this.balance = 0;
        this.withdrawValue = 0;
        this.transactionValue = 0;
        this.transferDestination = 0
    }

    setMemberStatus(member) {
        this.member = member;
    }

    setBalance(balance) {
        this.balance = balance;
    }

    setWithdrawValue(value) {
        this.withdrawValue = value
    }

}

class Teller extends Person {
    constructor(name, address) {
        super(name, address);
    }

    serve() {
        if (this.queueList.length == 1) { // Member will be served if only the queue list is just one person
            if (Customer.member) { // If the customer already a member
                Customer.setBalance(BankSystem.balance)

                switch (Customer.intention) {
                    case "withdraw":

                        if (BankSystem.balance > Customer.withdrawValue) {
                            BankSystem.withdraw(Customer.withdrawValue)
                        }

                        break;
                    case "deposit":

                        BankSystem.deposite(Customer.balance)

                        break;
                    case "check":

                        BankSystem.getBalance()

                        break;
                    case "balance":
                        BankSystem.getBalance()
                        break;
                    case "transfer":
                        if (Customer.transactionValue && Customer.transferDestination) {
                            BankSystem.tranfer(Customer.transactionValue, Customer.transferDestination))
                        }
                        else {
                            console.log('Plese input transfer destination correctly')
                        }

                        break;

                    default:
                        break;
                }

            } else {
                Customer.setMemberStatus(true); // if the customer not a member, they will be forwarded as a new member
                BankSystem.setBalance(50.000)
            }
        }
    }
}

class BankSystem extends Person {
    constructor() {
        super(name),
            this.queueList = [], // Transaction queue list
            this.balance = 0;
    }

    addQueueList(queuelist) {
        this.queueList.push(queuelist)
    }


    // Name and member's balance
    customerList() {
        constructor(name, balance) {
            super(name) : this.balance;
        }
    }

    getBalance() {
        console.log(`Your balance is ${this.balance}`)
    }

    withdraw(value) {
        this.balance - value;
        console.log(`Your succesful withdraw your balance now is ${this.balance}`)
    }

    deposite(value) {
        this.balance + value
        console.log(`Your succesful deposite your balance now is ${this.balance}`)
    }

    setBalance(balance) {
        this.balance = balance
    }

    tranfer(value, destination) {
        console.log(`You succesful transer to ${destination} with value ${value}`)
    }

}